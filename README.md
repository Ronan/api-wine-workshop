# api-wine-workshop

API for the wine workshop forked from : https://github.com/mathieuancelin/react-workshop

## Build Setup

``` bash
# clone project
git clone https://framagit.org/Ronan/api-wine-workshop.git

# install dependencies
npm install

# serve at localhost:3000
npm start
```
